 $('#carouselExamplefooter').on('#carouselExamplefooter slide.bs.carousel', function (e) {
            
              
                var $e = $(e.relatedTarget);
                var idx = $e.index();
                var itemsPerSlide = 5;
                var totalItems = $('#carouselExamplefooter .carousel-item').length;
                
                if (idx >= totalItems-(itemsPerSlide-1)) {
                    var it = itemsPerSlide - (totalItems - idx);
                    for (var i=0; i<it; i++) {
                        // append slides to end
                        if (e.direction=="left") {
                            $('#carouselExamplefooter .carousel-item').eq(i).appendTo('.carousel-inner');
                        }
                        else {
                            $('#carouselExamplefooter .carousel-item').eq(0).appendTo('.carousel-inner');
                        }
                    }
                }
            });
            
            
              $('#carouselExamplefooter').carousel({ 
                            interval: 4000
                    });
            
            
              $(document).ready(function() {
            /* show lightbox when clicking a thumbnail */
                $('#carouselExamplefooter a.thumb').click(function(event){
                  event.preventDefault();
                  var content = $('#carouselExamplefooter .modal-body');
                  content.empty();
                    var title = $(this).attr("#carouselExamplefooter title");
                    $('#carouselExamplefooter .modal-title').html(title);        
                    content.html($(this).html());
                    $("#carouselExamplefooter .modal-profile").modal({show:true});
                });
            
              });